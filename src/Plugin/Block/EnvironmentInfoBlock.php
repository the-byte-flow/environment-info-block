<?php

namespace Drupal\environment_info_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "environment_info_block",
 *   admin_label = @Translation("Environment Info Block"),
 *   category = @Translation("Environment Info Block")
 * )
 */
class EnvironmentInfoBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build['content'] = [
      '#theme' => 'basic_twig_block',
      '#data'=> [
        'environment' => getenv('APP_ENV'),
        'user' => getenv('DB_USER')
      ]
    ];
    return $build;
  }


}
